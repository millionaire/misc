- ![slack com ico](https://user-images.githubusercontent.com/5549677/36627955-ac6a5544-195b-11e8-9668-c4c169954a24.png)
 [`Slack`](https://uncdt.slack.com) ![jira](https://user-images.githubusercontent.com/5549677/36627968-d68acab6-195b-11e8-955d-3252c5739cef.jpg) [`JIRA`](http://docs-unc.ddns.net:8080/jira/secure/RapidBoard.jspa?rapidView=1) [`presentation`](https://docs.google.com/presentation/d/1zbrVEYxTWCZeuo7PQh3ya9iLSNkdz6FtE8jqCCPd3iI/edit#slide=id.gc6f9e470d_0_0) [`speaker-notes`](https://docs.google.com/document/d/1a1bAGzp85edJi0iba1c0T4LEvIfJUiYQrCuZwLIBziI/edit)

---

### Gitlab
- ![](https://user-images.githubusercontent.com/5549677/36627909-f7d816de-195a-11e8-9c4b-b6e2cd77efa6.png) [`spring`](https://gitlab.com/millionaire/spring) [`commits`](https://gitlab.com/millionaire/spring/commits/master) [`CI Pipelines`](https://gitlab.com/millionaire/spring/pipelines)
- ![](https://user-images.githubusercontent.com/5549677/36627908-f7a5f41a-195a-11e8-9b26-55bea16ce19a.jpeg) [`react`](https://gitlab.com/millionaire/millionaire-react) [`commits`](https://gitlab.com/millionaire/millionaire-react/commits/master) [`CI Pipelines`](https://gitlab.com/millionaire/millionaire-react/pipelines)
- ![](https://user-images.githubusercontent.com/5549677/36627907-f7775448-195a-11e8-9d13-7ba5c698ead3.png) [`todo`](https://gitlab.com/millionaire/misc/blob/master/todo.md) [`misc`](https://gitlab.com/millionaire/misc) [`docker-compose.yml`](https://gitlab.com/millionaire/misc/blob/master/docker-compose.yml) [`docs`](https://gitlab.com/millionaire/misc/blob/master/docs)

---
- [`https://milioner.ddns.net`](https://milioner.ddns.net)
- [`http://localhost:3000`](http://localhost:3000/)
---
- [`EC2 Console`](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Instances:sort=instanceId)
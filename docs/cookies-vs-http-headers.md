# Cookies vs HTTP Headers
- a problem occured with passing `access_token` via cookies. After ~1day of debugging I can't make work Fetch API to send cookies. A problem is on client side. `Postman` and `curl` are sends cookies correctly and recieves data from secured API `/some`.
- i've tried options but it isn't work:
    - `credentials: 'include'`
    - `credentials: 'same-origin'`

## Links to a problem
- js fetch dont send cookies
- [javascript - How to send cookies with node-fetch? - Stack Overflow](https://stackoverflow.com/questions/34815845/how-to-send-cookies-with-node-fetch#35258629)
- [javascript - How to send cookies with node-fetch? - Stack Overflow](https://stackoverflow.com/questions/34815845/how-to-send-cookies-with-node-fetch)
- [github/fetch: A window.fetch JavaScript polyfill.](https://github.com/github/fetch#sending-cookies)
- [Fetch API with Cookie - Stack Overflow](https://stackoverflow.com/questions/34558264/fetch-api-with-cookie)
- [Difficulty sending cookies with fetch requests in JavaScript - Stack Overflow](https://stackoverflow.com/questions/29662394/difficulty-sending-cookies-with-fetch-requests-in-javascript)
- [javascript - session does not save after fetch post request with express - Stack Overflow](https://stackoverflow.com/questions/35612226/session-does-not-save-after-fetch-post-request-with-express)
- [Trying to get then send a cookie using react and fetch : reactjs](https://www.reddit.com/r/reactjs/comments/4gszc2/trying_to_get_then_send_a_cookie_using_react_and/)
- [reactjs - Trying to get then send a cookie using react and fetch - Stack Overflow](https://stackoverflow.com/questions/36897364/trying-to-get-then-send-a-cookie-using-react-and-fetch)
- [Fetch() Doesn't Send Cookies By Default](https://esimmler.com/fetch-doesnt-send-cookies-by-default/)
- fetch credentials include doesn't send cookies
- [Introduction to fetch()  |  Web  |  Google Developers](https://developers.google.com/web/updates/2015/03/introduction-to-fetch)
- topics to research: `OPTION`, `pre-flight` request
    - [Руководство по кросс-доменным запросам (CORS)](http://grishaev.me/cors)
    - [Understanding cors](https://spring.io/understanding/cors)
- [cors - Fetch API default cross-origin behavior - Stack Overflow](https://stackoverflow.com/questions/44121783/fetch-api-default-cross-origin-behavior)
- [cors can fetch from postman but can't fetch from javascript at DuckDuckGo](https://duckduckgo.com/?q=cors+can+fetch+from+postman+but+can%27t+fetch+from+javascript&bext=msl&atb=v71-4__&ia=qa)

--- 

Now I will send cookies via HTTP-headers.
- [local storage - Is it safe to store a jwt in localStorage with reactjs? - Stack Overflow](https://stackoverflow.com/questions/44133536/is-it-safe-to-store-a-jwt-in-localstorage-with-reactjs)
- seems like i can't access headers from Fetch?
    - [javascript - Reading response headers with Fetch API - Stack Overflow](https://stackoverflow.com/questions/43344819/reading-response-headers-with-fetch-api)
    - [javascript - How to get headers of the response from fetch - Stack Overflow](https://stackoverflow.com/questions/38953864/how-to-get-headers-of-the-response-from-fetch)
    - you need to handle pre-flight `OPTION` request
    - **SOLUTION AFTER MANY HOURS OF STRUGGLING:** 
        - add `.cors().and()` to your configuration class that `extends WebSecurityConfigurerAdapter`
        - add exposedHeaders to your controller, now my controller is: `@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders="access_token")`


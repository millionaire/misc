App Website:

http://uncdt.ddns.net

---

reset questions:
```sh
curl -d '{"answerId":1}' -H 'Content-Type: application/json' http://uncdt.ddns.net:8092/api/rooms/1/answer
```

---

logs of spring app:

1. ssh into server
2. `docker logs -f millionaire_spring-app_1`

---

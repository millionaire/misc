# Database

---

Run MySQL container locally (w/o `docker-compose`)

```sh
docker run \
--name mysql_without_compose \
-p 3306:3306 \
--env MYSQL_ROOT_PASSWORD=password \
--env MYSQL_DATABASE=demo \
--env MYSQL_USER=demo_user \
--env MYSQL_PASSWORD=demo_pass \
mysql:5.7 \
--character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
```

---

add unique constraint on `name` column. 

```java
@Entity
public class User {

@Id
 @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique=true)
    @NotNull
 private String name;
```

Problem:

```
2018-02-12 10:52:22.979 ERROR 63166 --- [           main] org.hibernate.tool.hbm2ddl.SchemaExport  : HHH000389: Unsuccessful: alter table user add constraint UK_gj2fy3dcix7ph7k8684gka40c unique (name)
2018-02-12 10:52:22.979 ERROR 63166 --- [           main] org.hibernate.tool.hbm2ddl.SchemaExport  : Specified key was too long; max key length is 767 bytes
```
- [this solution](https://stackoverflow.com/questions/1814532/1071-specified-key-was-too-long-max-key-length-is-767-bytes#16820166) doesn't work for me. I've solved this by [updating `MySQL` to `5.7`](https://stackoverflow.com/questions/1814532/1071-specified-key-was-too-long-max-key-length-is-767-bytes)

--- 

# Get into MySQL CLI
1. ssh to server
2. get into mysql container shell: `docker exec -it mysql-db bash` where `mysql-db` is the name or hash of mysql container
3. `mysql --user=demo_user --password=demo_pass demo`

or oneliner:

```sh
docker exec -it mysql-db mysql --user=demo_user --password=demo_pass demo
```


## examples:

```
mysql> show tables;
+----------------+
| Tables_in_demo |
+----------------+
| answer         |
| answer_order   |
| category       |
| question       |
| question_type  |
| room           |
| room_questions |
| room_users     |
| saved_answer   |
| user           |
| user_role      |
+----------------+
11 rows in set (0.00 sec)
```

```
mysql> select * from user;
+----+-------+--------------------------------------------------------------+---------+--------------+
| id | name  | salted_hash                                                  | surname | user_role_id |
+----+-------+--------------------------------------------------------------+---------+--------------+
|  1 | vasek | $2a$10$ilNB1s8JI6HCmXBMaIxtfuZLcdNiZ0stQGb/oOXmgl5MzBBUZYm.e | pupkin  |            1 |
|  2 | ff    | $2a$10$mshL5/iB7y5HvDBOAPSJl.t1NmZW8TPlFL1FdAbkbKsc4VMGr3Ge. | ff      |            1 |
|  3 | abc   | $2a$10$prrUZovCNHkolmqM6tyCe.esc75d91Ss35x7/I/etZhynsDoRY/gC | abc     |            1 |
|  4 | abc   | $2a$10$Hj/vkgRQjogeNVs9W18XYO6lIKrBCzTvnfCLZFvpM7M0frY8NmJLi | abc     |            1 |
|  5 | abc   | $2a$10$wS0m36Drt974LuByz/1AjeBAxfYG/z2siObivcUj2nUzUhbwZb0e6 | abc     |            1 |
|  6 | ee    | $2a$10$ERSXSltEh5.soSWt2s5SY.KK4entQm9eX2Snc0Mntx1EUMCf9Yu8W | ee      |            1 |
|  7 | ee    | $2a$10$utT9MPSof/2vJqoHTugU5u8MEZQM5HTOqu7Ja/c9Q8gObXi5/WoFC | ee      |            1 |
|  8 | ee    | $2a$10$rXEEdTUqMVWHaarqzg7rXe1/zFF3Z7MhQAyNLTls94VV3JdxkhUnq | ee      |            1 |
|  9 | 12    | $2a$10$Ff63z14o9j2zathV2AcmyerlIJGUndhmaWLs8.mKzvhVmSNnFMaA6 | 12      |            1 |
| 10 | 12    | $2a$10$nk96kscYT7IgeWdGIYYAp.CmbXbjIHvzvjHnSZCXkRZmFAT5eYap2 | 12      |            1 |
| 11 | sveta | $2a$10$oppbfepg9ZBa6Ouy01MhVO2tjA4oeV9dMZQF7r.657FOCTR8miM0C | sveta   |            1 |
| 12 | t5    | $2a$10$IGmzipL9kuLfaIw//ghtf.6j3zAcH/IuTwyEgPttgXBPC1P7n2RpC | t5      |            1 |
| 13 | t5    | $2a$10$QuGEIXK4UbrNsE9dDahnLO2z9NZudl8w4ssHHF8ccvmFSNybivrbi | t5      |            1 |
| 14 | t5    | $2a$10$O395Sab/gIU9yVCvfSZTp.6Iovay46Mlp1pfulz.zxBX9QqdMfROy | t5      |            1 |
| 15 | t5    | $2a$10$pX.wd7JMJ/DNaHob/MvHCeKV470TyozvVnwMiy5dPlP/ZI/gHyjha | t5      |            1 |
| 16 | t5    | $2a$10$ecaiWkAKvP0jbHGmYMQ1xu2ckE8XzsdvzvZpjpntQladIDpwHu26O | t5      |            1 |
| 17 | t5    | $2a$10$nW4DU9OnNslolS/u7xU1GugQ12B.hLnMw8IWOjua7/x2pOSszgyNu | t5      |            1 |
+----+-------+--------------------------------------------------------------+---------+--------------+
17 rows in set (0.00 sec)
```

```
mysql> describe user;
+--------------+--------------+------+-----+---------+----------------+
| Field        | Type         | Null | Key | Default | Extra          |
+--------------+--------------+------+-----+---------+----------------+
| id           | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| name         | varchar(255) | NO   | UNI | NULL    |                |
| salted_hash  | varchar(255) | YES  |     | NULL    |                |
| surname      | varchar(255) | NO   |     | NULL    |                |
| user_role_id | bigint(20)   | YES  | MUL | NULL    |                |
+--------------+--------------+------+-----+---------+----------------+
5 rows in set (0.00 sec)
```

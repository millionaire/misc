# Security
- Basically we need to store hash and salt in user-table in db. But if you use spring security it's 1 column. The salt is incorporated into the hash.
    - [java - Do I need to store the salt with bcrypt? - Stack Overflow](https://stackoverflow.com/questions/277044/do-i-need-to-store-the-salt-with-bcrypt#277057)


there is `BCrypt` alternative: `PBKDF2`

---
